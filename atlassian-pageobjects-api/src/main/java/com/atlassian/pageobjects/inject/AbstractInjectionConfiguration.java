package com.atlassian.pageobjects.inject;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Abstract implementation of {@link com.atlassian.pageobjects.inject.InjectionConfiguration}.
 *
 * @since 2.1
 */
@ExperimentalApi
public abstract class AbstractInjectionConfiguration implements InjectionConfiguration
{
    protected final List<InterfaceToImpl> interfacesToImpls = new LinkedList<>();
    protected final List<InterfaceToInstance> interfacesToInstances = new LinkedList<>();

    protected static class InterfaceToImpl
    {
        public final Class<?> interfaceType;
        public final Class<?> implementation;

        private InterfaceToImpl(Class<?> interfaceType, Class<?> implementation)
        {
            this.interfaceType = requireNonNull(interfaceType, "interfaceType");
            this.implementation = requireNonNull(implementation, "implementation");
            if (!interfaceType.isAssignableFrom(implementation)) {
                throw new IllegalArgumentException("Implementation type " + implementation.getName() + " does not "
                        + "implement " + interfaceType.getName());
            }
        }
    }

    protected static class InterfaceToInstance
    {
        public final Class<?> interfaceType;
        public final Object instance;

        private InterfaceToInstance(Class<?> interfaceType, Object instance)
        {
            this.interfaceType = requireNonNull(interfaceType, "interfaceType");
            this.instance = requireNonNull(instance, "instance");
            if (!interfaceType.isInstance(instance)) {
                throw new IllegalArgumentException("Object " + instance + " does not "
                        + "implement " + interfaceType.getName());
            }
        }
    }

    @Override
    @Nonnull
    public final <I> InjectionConfiguration addImplementation(@Nonnull Class<I> interfaceType, @Nonnull Class<? extends I> implementationType)
    {
        requireNonNull(interfaceType, "interfaceType");
        requireNonNull(implementationType, "implementationType");
        interfacesToImpls.add(new InterfaceToImpl(interfaceType, implementationType));
        return this;
    }

    @Override
    @Nonnull
    public final <C, I extends C> InjectionConfiguration addSingleton(@Nonnull Class<C> type, @Nonnull I instance)
    {
        requireNonNull(type, "type");
        requireNonNull(instance, "instance");
        interfacesToInstances.add(new InterfaceToInstance(type, instance));
        return this;
    }


}
