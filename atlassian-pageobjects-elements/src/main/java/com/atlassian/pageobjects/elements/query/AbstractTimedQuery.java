package com.atlassian.pageobjects.elements.query;

import com.atlassian.pageobjects.elements.query.util.Clock;
import com.atlassian.pageobjects.elements.query.util.ClockAware;
import com.atlassian.pageobjects.elements.query.util.SystemClock;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.util.Clocks.getClock;
import static com.atlassian.pageobjects.elements.util.StringConcat.asString;
import static java.util.Objects.requireNonNull;


/**
 * <p>
 * Abstract query that implements {@link #byDefaultTimeout()} in terms of {@link #by(long)}, and {@link #by(long)} as a
 * template method calling the following hooks (to be implemented by subclasses):
 * <ul>
 * <li>{@link #currentValue()} - to determine current evaluation of the query
 * <li>{@link #shouldReturn(Object)} - which indicates, if current value of the query should be returned
 * </ul>
 * <p>
 * <p>
 * In addition, an {@link ExpirationHandler} must be provided to handle the case of expired query.
 *
 * @see ExpirationHandler
 */
@NotThreadSafe
public abstract class AbstractTimedQuery<T> extends AbstractPollingQuery implements TimedQuery<T>, ClockAware
{
    private final Clock clock;
    private final Poll<T> poll;

    protected AbstractTimedQuery(Clock clock, long defTimeout, long interval, ExpirationHandler expirationHandler)
    {
        super(interval, defTimeout);
        this.clock = requireNonNull(clock, "clock");
        this.poll = new Poll(this, this.clock())
                .every((int) interval, TimeUnit.MILLISECONDS)
                .until(new CustomMatcher())
                .onFailure(expirationHandler);
    }

    @Override
    public T call() throws Exception
    {
        return currentValue();
    }

    class CustomMatcher extends BaseMatcher<T>
    {
        @Override
        public boolean matches(Object o)
        {
            return AbstractTimedQuery.this.shouldReturn((T) o);
        }

        @Override
        public void describeTo(Description description)
        {

        }
    }

    protected ExpirationHandler expirationHandler()
    {
        return this.poll.expirationHandler;
    }

    protected AbstractTimedQuery(long defTimeout, long interval, ExpirationHandler expirationHandler)
    {
        this(SystemClock.INSTANCE, defTimeout, interval, expirationHandler);
    }

    protected AbstractTimedQuery(PollingQuery other, ExpirationHandler expirationHandler)
    {
        this(getClock(other), other.defaultTimeout(), requireNonNull(other, "other").interval(), expirationHandler);
    }

    /**
     * Checks the condition immediately, once every interval, and finally once after timeout reached.
     * If the condition ever passes, return immediately
     *
     * @param timeout in milliseconds (ms) to wait for the condition to pass
     */
    public final T by(long timeout)
    {
        try
        {
            return poll.withTimeout((int) timeout, TimeUnit.MILLISECONDS).call();
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public final T by(long timeout, TimeUnit unit)
    {
        return by(TimeUnit.MILLISECONDS.convert(timeout, unit));
    }

    public T byDefaultTimeout()
    {
        return by(defaultTimeout);
    }

    public final T now()
    {
        return by(1, TimeUnit.MILLISECONDS);
    }

    /**
     * If the current evaluated query value should be returned.
     *
     * @param currentEval current query evaluation
     *                    expires
     * @return <code>true</code>, if the current query evaluation should be returned as a result of this timed query
     */
    protected abstract boolean shouldReturn(T currentEval);

    /**
     * Current evaluation of the query.
     *
     * @return current evaluation of the query
     */
    protected abstract T currentValue();

    public Clock clock()
    {
        return clock;
    }

    @Override
    public String toString()
    {
        return asString(getClass().getName(), "[interval=", interval, ",defaultTimeout=", defaultTimeout, "]");
    }
}
