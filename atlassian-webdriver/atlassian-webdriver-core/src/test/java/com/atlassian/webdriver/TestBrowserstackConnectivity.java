package com.atlassian.webdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.Request;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.handler.AbstractHandler;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.ServerSocket;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestBrowserstackConnectivity {
    private static final Logger log = LoggerFactory.getLogger(TestBrowserstackConnectivity.class);

    private Server server;
    private int port;

    @Before
    public void setup() {
        port = pickFreePort();
        server = buildServer(port);
    }

    @After
    public void cleanup() throws Exception {
        if (server != null && server.isRunning()) {
            server.stop();
        }
    }

    @Test
    public void failsWithoutProperCredentials() throws Exception {
        BrowserstackWebDriverBuilder builder = new BrowserstackWebDriverBuilder("fake", "fake")
                .withBrowser("ie")
                .withBrowserVersion("10")
                .withOS("Windows")
                .withOSVersion("7");

        try {
            server.start();
            final AtlassianWebDriver driver = builder.build();
            driver.get("http://localhost:" + port);
            final String result = driver.findElement(By.tagName("h1")).getText();
            assertThat(result, equalTo("Hello"));
            assertThat("Should have thrown an authentication error", true, equalTo(false));
        } catch (Exception e) {
            log.debug("Caught an exception, as expected", e);
            assertThat(e.getMessage(), containsString("Invalid username or password"));
        } finally {
            server.stop();
        }
    }

    private Server buildServer(int port) {
        Handler handler = new AbstractHandler() {
            public void handle(String target, HttpServletRequest request, HttpServletResponse response, int dispatch)
                    throws IOException {
                response.setContentType("text/html");
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println("<h1>Hello</h1>");
                ((Request) request).setHandled(true);
            }
        };

        Server server = new Server(port);
        server.setHandler(handler);
        return server;
    }

    private static int pickFreePort() {
        ServerSocket socket = null;
        try {
            socket = new ServerSocket(0);
            return socket.getLocalPort();
        } catch (IOException e) {
            throw new RuntimeException("Error opening socket", e);
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    throw new RuntimeException("Error closing socket", e);
                }
            }
        }
    }

}
