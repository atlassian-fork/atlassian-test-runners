package com.atlassian.webdriver.it.tests;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.RequireBrowser;
import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.IncludedScriptErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.NoErrorsPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.UntypedErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.WindowErrorPage;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import com.google.common.collect.ImmutableSet;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule.ERROR_SEPARATOR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;

/**
 * Test the rule for logging client-side console output.
 *
 * At the time of writing, the logging only captures exceptions.
 * The tests only work in Firefox, since the rule uses a Firefox extension to get the output.
 */
@RequireBrowser(Browser.FIREFOX)
public class TestCapturingJavaScriptConsoleOutput extends AbstractSimpleServerTest
{
    private static final String UNCAUGHT_EXCEPTION_THROW_STRING = "uncaught exception: throw string";

    private JavaScriptErrorsRule rule;

    @Before
    public void setUp()
    {
        rule = new JavaScriptErrorsRule();
    }

    @Test
    public void testPageWithNoErrors()
    {
        product.visit(NoErrorsPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, equalTo(""));
    }

    @Test
    public void testSingleErrorInWindowScope()
    {
        final Page page = product.visit(WindowErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("ReferenceError: foo is not defined"));
        assertThat(consoleOutput, containsString(page.getUrl()));
    }

    @Test
    public void testErrorIncludesLineAndColumnInfo() {
        final Page page = product.visit(WindowErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString(page.getUrl() + ":11:13"));
    }

    @Test
    public void testMultipleErrorsInIncludedScripts()
    {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("TypeError: $ is not a function"));
        assertThat(consoleOutput, containsString(page.objectIsNotFunctionScriptUrl()));

        assertThat(consoleOutput, containsString("Error: throw Error('bail')"));
        assertThat(consoleOutput, containsString(page.throwErrorObjectScriptUrl()));
    }

    @Test
    public void testEachJSErrorIsOnADoubleNewLine()
    {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        final String[] errors = consoleOutput.split(ERROR_SEPARATOR);

        assertThat(errors.length, equalTo(3));
        // We get “unreachable code after return statement” warnings from Firefox 40 onwards;
        // see <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/return$revision/945829#Automatic_Semicolon_Insertion>.
        assertThat(errors[0], containsString("jquery-1.4.2.js"));
        assertThat(errors[1], containsString(page.objectIsNotFunctionScriptUrl()));
        assertThat(errors[2], containsString(page.throwErrorObjectScriptUrl()));
    }

    @Test
    public void testErrorsIncludeCategories() {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        final String[] errors = consoleOutput.split(ERROR_SEPARATOR);

        assertThat(errors[0], stringContainsInOrder(Lists.newArrayList("Warning","unreachable code after return statement")));
        assertThat(errors[1], stringContainsInOrder(Lists.newArrayList("Exception","TypeError: $ is not a function")));
        assertThat(errors[2], stringContainsInOrder(Lists.newArrayList("Exception","Error: throw Error('bail')")));
    }

    @Test
    public void testErrorsIncludeStackTraces() {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();

        assertThat(consoleOutput, stringContainsInOrder(Lists.newArrayList(
                "Stack:",
                "@http://localhost", /* port info */ page.objectIsNotFunctionScriptUrl(), ":3:15",
                "\n",
                "@http://localhost", /* port info */ page.objectIsNotFunctionScriptUrl(), ":1:1"
        )));
        assertThat(consoleOutput, stringContainsInOrder(Lists.newArrayList(
                "Stack:",
                "@http://localhost", /* port info */ page.throwErrorObjectScriptUrl(), ":3:1",
                "\n",
                "@http://localhost", /* port info */ page.throwErrorObjectScriptUrl(), ":1:1"
        )));
    }

    @Test
    public void testCanCaptureUntypedErrors()
    {
        product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString(UNCAUGHT_EXCEPTION_THROW_STRING));
    }

    @Test
    public void testCanBeOverriddenToIgnoreSpecificErrors()
    {
        rule = rule.errorsToIgnore(ImmutableSet.of(UNCAUGHT_EXCEPTION_THROW_STRING));
        product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, not(containsString(UNCAUGHT_EXCEPTION_THROW_STRING)));
    }

    @Ignore("The JSErrorCollector plugin currently cannot capture console errors")
    @Test
    public void testCanCaptureConsoleErrors()
    {
        final UntypedErrorPage page = product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("console.error"));
        assertThat(consoleOutput, containsString(page.consoleErrorScriptUrl()));
    }

    @Test
    public void testCurrentlyCannotCaptureConsoleErrors()
    {
        final UntypedErrorPage page = product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, not(containsString("console.error")));
        assertThat(consoleOutput, not(containsString(page.consoleErrorScriptUrl())));
    }
}
