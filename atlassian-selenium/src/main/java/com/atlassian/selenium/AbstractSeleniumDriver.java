package com.atlassian.selenium;

import static java.util.Objects.requireNonNull;

/**
 * Base class for utilities using {@link SeleniumClient} for higher-level operations.
 *
 * @since v1.21
 */
public abstract class AbstractSeleniumDriver
{
    protected final SeleniumClient client;


    protected AbstractSeleniumDriver(final SeleniumClient client)
    {
        this.client = requireNonNull(client, "client");
    }
}
